#!/usr/bin/env python

# ztranslate - pythonic simple translater
# Copyright (C) 2011  Vladimir Simakhin
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
    
import sys, os
import gobject, gtk
import urllib2
import simplejson
from urllib2 import quote
from BeautifulSoup import BeautifulSoup
from BeautifulSoup import BeautifulStoneSoup

#globals
icon_name = os.path.dirname(sys.argv[0]) + "/ztranslate.png"

#------------------------------------------------
#class  ZClipboard
#descr  the clipboard class
#------------------------------------------------
class ZClipboard:
  #------------------------------------------------
  #function fetch_clipboard
  #descr:   fetchs the clipboard every x seconds
  #------------------------------------------------
  def fetch_clipboard(self):
    if self.ztranslate.is_visible and self.ztranslate.is_typing == False:
      self.clipboard.request_text(self.clipboard_text_received)
    
    gobject.timeout_add(self.ztranslate.fetch_period, self.fetch_clipboard)

  #------------------------------------------------
  #function clipboard_text_received
  #descr:   fires if text from clipboard is received
  #------------------------------------------------
  def clipboard_text_received(self, clipboard, text, data):
    if text != self.ztranslate.input_entry.get_text() and text:
      if len(text)>1:
        gobject.idle_add(self.ztranslate.input_entry.set_text, text.strip())
    
  def __init__(self, ztranslate):
    self.ztranslate = ztranslate
    
    self.clipboard = gtk.clipboard_get(gtk.gdk.SELECTION_PRIMARY)
    self.fetch_clipboard()

#------------------------------------------------
#class  ZSettings
#descr  the settings class
#------------------------------------------------
class ZSettings:
  widget_file = os.path.dirname(sys.argv[0]) + "/zsettings.glade"
  
  #------------------------------------------------
  #function fill_in_language_hash
  #descr:   fills in the language hash
  #------------------------------------------------
  def fill_in_language_hash(self):
    self.lang_hash = {}
    
    if self.ztranslate.translate_service == "slovnik":
      self.lang_hash['Czech'] = 'cz'
      self.lang_hash['Russian'] = 'ru'
      self.lang_hash['English'] = 'en'
      self.lang_hash['German'] = 'de'
      self.lang_hash['Spain'] = 'sp'
      self.lang_hash['Italian'] = 'it'
      self.lang_hash['French'] = 'fr'
    elif self.ztranslate.translate_service == "google":
      self.lang_hash['Czech'] = 'cs'
      self.lang_hash['Russian'] = 'ru'
      self.lang_hash['English'] = 'en'
      self.lang_hash['German'] = 'de'
      self.lang_hash['Spain'] = 'sp'
      self.lang_hash['Italian'] = 'it'
      self.lang_hash['French'] = 'fr'
   
  #------------------------------------------------
  #function fill_in_service_hash
  #descr:   fills in the service hash
  #------------------------------------------------
  def fill_in_service_hash(self):
    self.service_hash = {}
    self.service_hash['slovnik.cz'] = 'slovnik'
    self.service_hash['translate.google.com'] = 'google'
    
  def on_window_destroy(self, widget, data = None):
    pass
  
  #------------------------------------------------
  #function on_ok_button_click
  #descr:   fires on OK button click
  #in:      standard GTK
  #------------------------------------------------
  def on_ok_button_click(self, widget, data = None):
    #translate service
    self.ztranslate.translate_service = self.service_hash[self.translate_service.get_active_text()]
    self.fill_in_language_hash()
    
    #fetch period
    if self.fetch_period_entry.get_text().isdigit():
      self.ztranslate.fetch_period = int(float(self.fetch_period_entry.get_text())*1000)
    else:
      self.ztranslate.fetch_period = 2000
    
    #languages
    self.ztranslate.lng_from = self.lang_hash[self.lng_from.get_active_text()]
    self.ztranslate.lng_to = self.lang_hash[self.lng_to.get_active_text()]

    self.window.destroy()

  #------------------------------------------------
  #function __init__
  #descr:   constructor
  #------------------------------------------------
  def __init__(self, ztranslate):
    self.ztranslate = ztranslate
    
    #fill in hashes
    self.fill_in_language_hash()
    self.fill_in_service_hash()
    
    #init builder
    self.builder = gtk.Builder()
    self.builder.add_from_file(self.widget_file)
      
    #settings window
    self.window = self.builder.get_object("window")
    self.fetch_period_entry = self.builder.get_object("fetch_period_entry")
    self.ok_button = self.builder.get_object("ok_button")
    self.lng_from = self.builder.get_object("lng_from")
    self.lng_to = self.builder.get_object("lng_to")
    self.translate_service = self.builder.get_object("translate_service")
    
    #create language liststore and fill it in
    self.language_liststore = gtk.ListStore(str)
    for key in self.lang_hash:
      self.language_liststore.append([key])
    
    #create service liststore and fill it in
    self.service_liststore = gtk.ListStore(str)
    for key in self.service_hash:
      self.service_liststore.append([key])
      
    self.lng_from.set_model(self.language_liststore)
    self.lng_to.set_model(self.language_liststore)
    self.lng_from.set_text_column(0)
    self.lng_to.set_text_column(0)
    
    self.translate_service.set_model(self.service_liststore)
    self.translate_service.set_text_column(0)
    
    #signals
    self.window.connect("destroy", self.on_window_destroy)
    self.ok_button.connect("clicked", self.on_ok_button_click)
    
    #set values
    self.fetch_period_entry.set_text(str(self.ztranslate.fetch_period/1000.0))
    
    for key in self.lang_hash:
      if self.lang_hash[key] == self.ztranslate.lng_from:
        self.lng_from.child.set_text(key)  
      if self.lang_hash[key] == self.ztranslate.lng_to:
        self.lng_to.child.set_text(key)
    
    for key in self.service_hash:
      if self.service_hash[key] == self.ztranslate.translate_service:
        self.translate_service.child.set_text(key)
        
    #show
    self.window.show_all()
    
#------------------------------------------------
#class  ZTranslate
#descr  the main class
#------------------------------------------------
class ZTranslate:
  widget_file = os.path.dirname(sys.argv[0]) + "/ztranslate.glade"
  config_file = os.path.dirname(sys.argv[0]) + "/ztranslate.conf"
  is_visible = True #True when main window is visible
  is_typing = False #True when user types something in the input entry
  fetch_period = 2000 #Fetch period in ms to check clipboard
  lng_from = "cs" #translation language (from)
  lng_to = "ru" #translation language (to)
  translate_service = "google"
  
  #------------------------------------------------
  #function on_window_destroy
  #descr:   fires on closing the main window
  #in:      standard GTK variables
  #------------------------------------------------
  def on_window_destroy(self, widget, data = None):
    self.on_status_icon_click(widget, data)
    return True

  #------------------------------------------------
  #function on_main_quit
  #descr:   fires on closing program
  #in:      standard GTK variables
  #------------------------------------------------
  def on_main_quit(self, widget, data = None):
    self.save_config()
    gtk.main_quit()
    
  #------------------------------------------------
  #function save_config
  #descr:   saves config file
  #------------------------------------------------  
  def save_config(self):
    #open config file
    f = 0
    try:
      f = open(self.config_file, 'w')
    except:
      print "Cannot open file", self.config_file
      
    if f:
      #write config
      f.write("<ztranslate>")
      f.write("<fetch_period>" + str(self.fetch_period) + "</fetch_period>")
      f.write("<translate_service>" + self.translate_service + "</translate_service>")
      f.write("<lng_from>" + self.lng_from + "</lng_from>")
      f.write("<lng_to>" + self.lng_to + "</lng_to>")
      f.write("</ztranslate>")
    
      #close file
      f.close()
  
  #------------------------------------------------
  #function load_config
  #descr:   loads config
  #------------------------------------------------ 
  def load_config(self):    
    #read the config file
    f = 0
    try:
      f = open(self.config_file, 'r')
    except:
      print "Cannot open file", self.config_file
      
    if f:
      soap = BeautifulStoneSoup(f.read())
      
      #close file
      f.close()

      if soap.find('lng_from'):
        self.lng_from = soap.find('lng_from').string
      
      if soap.find('lng_to'): 
        self.lng_to = soap.find('lng_to').string
      
      if soap.find('fetch_period'):
        self.fetch_period = int(soap.find('fetch_period').string)
      
      if soap.find('translate_service'):
        self.translate_service = soap.find('translate_service').string
    
  #------------------------------------------------
  #function translate_slovnik
  #descr:   translates text within slovnik.cz
  #in:      text - the text tha will be translated
  #out:     hash with translates
  #------------------------------------------------
  def translate_slovnik(self, text):
    in_word = False
    in_translated = False
    
    #empty hash for dictionary
    dictionary = {}
    
    #initial data
    url = "http://slovnik.seznam.cz/?lang="+self.lng_from+"_"+self.lng_to+"&q="
    page = ""
    
    #let's open html page
    try:
      page_handle = urllib2.urlopen(url+quote(text.encode("utf8", ":/")),timeout=1)
      page = page_handle.read()
      page_handle.close()
    except:
      print "Cannot open ", url+text
      return dictionary
    
    #parsing, something weird but it's working :)
    for line in page.split("\n"):
      if line.find('<td class="word">') >= 0: in_word = True    #one of the variants of text
      if line.find('<td class="translated">') >= 0: in_translated = True    #translations for variants
      if line.find('</td>') >= 0:
        in_word = False
        in_translated = False
      
      sline = line.strip()  
      if sline != "":
        if in_word and sline.find('<a href=') >= 0 and sline.find('<img') < 0:
          tag_position = sline.find('>')
          word = sline[tag_position+1:-4]
          #add key
          dictionary[word] = []
          
        if in_translated and sline.find('<a href=') >= 0 and sline.find('<img') < 0:
          tag_position = sline.find('>')
          translated = sline[tag_position+1:-4]
          #add value for key
          dictionary[word].append(translated)
          
    return dictionary
  
  #------------------------------------------------
  #function translate_google
  #descr:   translates text within translate.google.com
  #in:      text - the text tha will be translated
  #out:     hash with translates
  #------------------------------------------------
  def translate_google(self, text):
    #empty hash for dictionary
    dictionary = {}
    
    #url for translation
    url = "http://translate.google.ru/translate_a/t?client=x&sl=" + \
        self.lng_from + "&tl=" + self.lng_to + "&hl=en&text=" + quote(text.encode("utf8", ":/"))
    
    #let's try to get the data
    try:
      opener = urllib2.build_opener()
      opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.36 (KHTML, like Gecko) Chrome/13.0.766.0 Safari/534.36')]
      translated_page = opener.open(url)
    except:
      print "Cannot open url", url
      return dictionary

    res = simplejson.loads(str(BeautifulSoup(translated_page)))
    
    #let's parse
    for x in res:
      if x == "sentences":  #usual translation
        dictionary["translation"] = []
        dictionary["translation"].append(res["sentences"][0]["trans"])
 
      if x == "dict":   #variants of translation
        for i in range(0,len(res["dict"])):
          dictionary[res["dict"][i]["pos"]] = []
          for y in res["dict"][i]["terms"]:
            dictionary[res["dict"][i]["pos"]].append(y)
            
    return dictionary

  #------------------------------------------------
  #function fill_in_output_textview
  #descr:   fills in the list with translations
  #in:      dictionary - hash with translation
  #------------------------------------------------    
  def fill_in_output_textview(self, dictionary):
    #clear the old translations
    self.textbuffer.set_text("")
    
    if not dictionary.values():
      self.textbuffer.set_text("not found :(")
    else:  
      for key in dictionary.keys():
        iter = self.textbuffer.get_end_iter()
        self.textbuffer.insert(iter, key+"\n")
      
        for item in dictionary[key]:
          iter = self.textbuffer.get_end_iter()
          self.textbuffer.insert(iter, "\t"+item+"\n")
    
  #------------------------------------------------
  #function on_input_entry_change
  #descr:   fires on entry field change
  #in:      standard GTK variables
  #------------------------------------------------
  def on_input_entry_change(self, widget, data = None):
    if self.is_typing != True:
      if self.translate_service == "slovnik":
        dic = self.translate_slovnik(self.input_entry.get_text())
      else:
        dic = self.translate_google(self.input_entry.get_text())

      self.fill_in_output_textview(dic)
      
      #write new text to clipboard
      clipboard = gtk.clipboard_get(gtk.gdk.SELECTION_PRIMARY)
      clipboard.set_text(self.input_entry.get_text())
      
  #------------------------------------------------
  #function on_input_entry_key_press
  #descr:   fires on any key press
  #in:      standard GTK variables
  #out:     none
  #------------------------------------------------
  def on_input_entry_key_press(self, widget, event, data = None):
    if event.keyval == 65293: #pressed enter
      self.is_typing = False
      self.on_input_entry_change(widget, data = None)
    else:
      self.is_typing = True
            
  #------------------------------------------------
  #function on_menu_settings_click
  #descr:   fires on settings menu right click
  #in:      standard GTK variables
  #out:     none
  #------------------------------------------------
  def on_menu_settings_click(self, widget, data = None):
    zsettings = ZSettings(self)
    
  #------------------------------------------------
  #function on_menu_exit_click
  #descr:   fires on exit
  #in:      standard GTK
  #------------------------------------------------
  def on_menu_exit_click(self, widget, data = None):
    self.on_main_quit(widget, data)
      
  #------------------------------------------------
  #function on_status_icon_click
  #descr:   fires on icon click
  #in:      standard GTK
  #------------------------------------------------
  def on_status_icon_click(self, widget, data = None):
    #hide or show window
    if self.is_visible:
      self.window.hide()
    else:
      self.window.show()
    
    self.is_visible = not self.is_visible
  
  #------------------------------------------------
  #function on_status_popup_menu
  #descr:   fires on popup menu click
  #in:      standard GTK
  #------------------------------------------------
  def on_status_popup_menu(self, widget, button, activate_time, data = None):
    menu = gtk.Menu()
    
    #create item Settings
    item = gtk.MenuItem("Settings")
    item.connect("activate", self.on_menu_settings_click)
    item.show()
    menu.append(item)
    
    #create separator
    item = gtk.SeparatorMenuItem()
    item.show()
    menu.append(item)
    
    #create item Exit
    item = gtk.MenuItem("Exit")
    item.connect("activate", self.on_menu_exit_click)
    item.show()
    menu.append(item)
    
    #show menu
    menu.popup(None, None, None, button, activate_time, None)

  #------------------------------------------------
  #function on_change_direction_click
  #descr:   fires on change direction button click
  #in:      standard GTK 
  #------------------------------------------------
  def on_change_direction_click(self, widget, data = None):
    tmp = self.lng_from
    self.lng_from = self.lng_to
    self.lng_to = tmp
    
    #let's tell user about changing the direction
    self.textbuffer.set_text(self.lng_from + " --> " + self.lng_to)
  
  #------------------------------------------------
  #function __init__
  #descr:   constructor
  #------------------------------------------------
  def __init__(self):
    #load config
    self.load_config()
    
    builder = gtk.Builder()
    builder.add_from_file(self.widget_file)
    
     #create window
    self.window = builder.get_object("window")
    self.window.set_keep_above(True)

    #controls
    self.input_entry = builder.get_object("input_entry")
    self.output_textview = builder.get_object("output_textview")
    self.textbuffer = self.output_textview.get_buffer()
    self.vs = builder.get_object("vs")
    self.vs.set_adjustment (self.output_textview.get_vadjustment())
    self.change_direction = builder.get_object("change_direction_button")
    
    #status icon
    self.status_icon = gtk.StatusIcon()
    self.status_icon.set_from_file(icon_name)
    self.status_icon.set_visible(True)
    
    #signals
    self.window.connect("delete-event", self.on_window_destroy)
    self.input_entry.connect("changed", self.on_input_entry_change)
    self.input_entry.connect("key-press-event", self.on_input_entry_key_press)
    self.status_icon.connect("activate", self.on_status_icon_click)    
    self.status_icon.connect("popup-menu", self.on_status_popup_menu) 
    self.change_direction.connect("clicked", self.on_change_direction_click)
    
    #show
    self.window.show_all()

def main():
  ztranslate = ZTranslate()
  zclipboard = ZClipboard(ztranslate)
  gtk.main()

if __name__ == "__main__":
  main()
